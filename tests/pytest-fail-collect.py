#! /usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2023 Arm Limited and/or its affiliates <open-source-office@arm.com>
# SPDX-License-Identifier: MIT

# This is a test case that fails to be collected

raise Exception("This test failed to collect")
